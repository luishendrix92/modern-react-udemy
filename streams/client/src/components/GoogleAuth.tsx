import * as React from "react";
import {connect, ConnectedProps} from "react-redux";

import {signIn, signOut} from "../actions";
import {RootState} from "../reducers";

const connector = connect(
  (state: RootState) => ({
    isSignedIn: state.auth.isSignedIn,
    userId: state.auth.userId
  }),
  { signIn, signOut }
);

type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {};

const clientId = "341093449301-p7q2i6bslfku4a29epnkhhnlb4d2l1rp.apps.googleusercontent.com";

class GoogleAuth extends React.Component<Props, {}> {
  private auth: gapi.auth2.GoogleAuth | undefined;

  componentDidMount(): void {
    gapi.load("client:auth2", async () => {
      await gapi.client.init({ scope: "email", clientId });

      this.auth = gapi.auth2.getAuthInstance();
      this.onAuthChange(this.auth.isSignedIn.get());
      this.auth.isSignedIn.listen(this.onAuthChange);
    });
  }

  onAuthChange = (isSignedIn: boolean): void => {
    if (isSignedIn) {
      this.props.signIn(
        this.auth?.currentUser.get().getId() || null
      );
    } else {
      this.props.signOut();
    }
  };

  onSignInClick = (): void => {
    this.auth?.signIn();
  };

  onSignOutClick = (): void => {
    this.auth?.signOut();
  };

  renderAuthButton() {
    if (this.props.isSignedIn === null) {
      return null;
    } else if (this.props.isSignedIn) {
      return (
        <button className="ui red google button" onClick={this.onSignOutClick}>
          <i className="google icon"/>
          Sign Out
        </button>
      );
    } else {
      return (
        <button className="ui blue google button" onClick={this.onSignInClick}>
          <i className="google icon"/>
          Sign in
        </button>
      );
    }
  }

  render() {
    return <>{this.renderAuthButton()}</>;
  };
}

export default connector(GoogleAuth);
