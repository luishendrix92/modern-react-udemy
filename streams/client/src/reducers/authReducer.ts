import {AuthAction, SIGN_IN, SIGN_OUT} from "../actions";

export type UserId = string | null;

type AuthState = {
  isSignedIn: boolean | null,
  userId: UserId
};

const INITIAL_STATE: AuthState = {
  isSignedIn: null,
  userId: null
};

function authReducer(state: AuthState = INITIAL_STATE, action: AuthAction): AuthState {
  switch (action.type) {
    case SIGN_IN:
      return { ...state, isSignedIn: true, userId: action.payload };
    case SIGN_OUT:
      return { ...state, isSignedIn: false, userId: null };
    default:
      return state;
  }
}

export default authReducer;
