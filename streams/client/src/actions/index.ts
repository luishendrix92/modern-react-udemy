import {UserId} from "../reducers/authReducer";

export const SIGN_IN = "SIGN_IN";
export const SIGN_OUT = "SIGN_OUT";

type SignInAction = {
  type: typeof SIGN_IN,
  payload: UserId,
};

type SignOutAction = {
  type: typeof SIGN_OUT
};

export type AuthAction = SignInAction | SignOutAction;

export function signIn(userId: UserId): AuthAction {
  return {
    type: SIGN_IN,
    payload: userId
  };
}

export function signOut(): AuthAction {
  return {
    type: SIGN_OUT
  };
}
