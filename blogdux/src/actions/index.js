import placeholder from "../apis/placeholder";
import _ from "lodash";

export function fetchPosts() {
  return async function(dispatch) {
    const response = await placeholder.get("/posts");

    dispatch({
      type: "FETCH_POSTS",
      payload: response.data
    });
  }
}

export function fetchUser(id) {
  return async function(dispatch) {
    const response = await placeholder.get(`/users/${id}`);

    dispatch({
      type: "FETCH_USER",
      payload: response.data
    });
  }
}

export function fetchPostsAndUsers() {
  return async function(dispatch, getState) {
    await dispatch(fetchPosts());

    _.chain(getState().posts)
      .map("userId")
      .uniq()
      .forEach((userId) => dispatch(fetchUser(userId)))
      .value();
  }
}
