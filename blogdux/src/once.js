/**
 * Ensures that a void function gets called only once.
 * @param procedure
 * @param log
 */
export default function once(procedure, log = false) {
  const argCache = {};

  return function __once(...args) {
    const argKey = JSON.stringify(args);
    const cacheHit = argCache.hasOwnProperty(argKey);

    if (!cacheHit) {
      argCache[argKey] = procedure(...args);
      return argCache[argKey];
    }

    if (log) {
      console.log(`[K] Once: ${procedure.name} ${cacheHit ? "hit cache" : "called once"} with arguments: ${argKey}`);
    }

    // Void Thunk
    return () => {};
  }
}
