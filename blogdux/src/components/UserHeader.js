import React from "react";
import {connect} from "react-redux";

class UserHeader extends React.Component {
  render() {
    if (!this.props.user) {
      return null;
    }

    return (
      <div>
        {this.props.user.name}
      </div>
    );
  }
}

const mapStateToProps = ({ users }, ownProps) => ({
  user: users.find(({ id }) => id === ownProps.userId)
});

export default connect(mapStateToProps)(UserHeader);
